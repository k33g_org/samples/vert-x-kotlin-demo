# vert-x-kotlin-demo

```
curl -G https://start.vertx.io/starter.zip \
     -d "groupId=garden.bots" \
     -d "artifactId=hello" \
     -d "vertxVersion=4.0.0-SNAPSHOT" \
     -d "vertxDependencies=vertx-web" \
     -d "language=kotlin" \
     -d "jdkVersion=11" \
     -d "buildTool=maven" \
     --output hello.zip

unzip hello.zip; rm hello.zip
```
